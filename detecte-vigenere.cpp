//
// Created by antoine on 21/09/18.
//


#include "utils.h"
#include "freqs.h"

using namespace std;

int main(int argc, char *argv[]) {

    vector<float> result;
    string texteChiffre = readInput(cin);

    vector<float> freqFr = frequenciesFrench();
    vector<float> freqRd = frequenciesRandom();
    //float icFr = coincidence( freqFr );
    //float icRd = coincidence( freqRd );
    for(unsigned int i=0; i<texteChiffre.length() /10 ;i++){
        unsigned int nbFr = 0;

        for(unsigned int shift=0; shift < i; shift++){
            string subTexteChiffre = subtext(texteChiffre, i, shift);

            if(isChiffrementMonoAlphabetique(subTexteChiffre))
                nbFr++;

        }

        if(nbFr == i) {
            vector<float> freq = frequencies( texteChiffre);
            vector<char> cle(i);


            for (unsigned int j = 0; j < i; j++) {

                // on cherche le caractere c entre A et Z qui maximise la coincidence mutuelle avec freqFr
                char res = 'A';
                float icMutuelleMax = 0.0;
                for(unsigned int c=0; c < 26; c++)
                {
                    float icMutuelle = coincidenceMutuelle(freq,freqFr,c);
                    if(icMutuelleMax < icMutuelle) {
                        icMutuelleMax = icMutuelle;
                        res = c;
                    }

                }
                cle[j] = res + 'A';

                cout << res;
            }

            //cout << cle << endl;
        }

    }

    /*pour n de 1 a s.size() / 10 faire

            nb_fr = 0
    pour shift de 0 a n-1 faire
    vector<float> freq = frequencies( substr( s, n, shift )
    Ic = coincidence( freq )
    Si Ic est plus proche de Ic_fr que de Ic_rd alors nb_fr++
    Si nb_fr == n alors // c'est probalement la bonne taille
    vector<char> cle( n )
    pour i de 0 a n-1 faire
    on cherche le caractere c entre A et Z qui maximise la coincidence mutuelle avec freq_fr
    cle[ i ] = c
    affiche( cle )
*/
    return 0;
}
