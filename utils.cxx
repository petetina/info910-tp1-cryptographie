#include <string>
#include <iostream>
#include <sstream>
#include "utils.h"
#include "freqs.h"

using namespace std;

string readInput( istream & in )
{
  ostringstream s( ostringstream::out );
  while ( in.good() )
    {
      char c;
      in >> c;
      s << c;
    }
  return s.str();
}

string filter_az( const std::string & s )
{
  ostringstream s2( ostringstream::out );
  for ( unsigned int i = 0; i < s.size(); ++i )
    {
      char c = s[ i ];
      if ( ( c >= 'a' ) && ( c <= 'z' ) )
	s2 << c;
      else if ( ( c >= 'A' ) && ( c <= 'Z' ) )
	s2 << (char) ( c-'A'+'a' );
    }
  return s2.str();
}

string filter_AZ( const std::string & s )
{
  ostringstream s2( ostringstream::out );
  for ( unsigned int i = 0; i < s.size(); ++i )
    {
      char c = s[ i ];
      if ( ( c >= 'a' ) && ( c <= 'z' ) )
	s2 << (char) ( c-'a'+'A' );
      else if ( ( c >= 'A' ) && ( c <= 'Z' ) )
	s2 << c;
    }
  return s2.str();
}

string subtext( const string & s, int every, int shift )
{
  ostringstream s2( ostringstream::out );
  for ( unsigned int i = shift; i < s.size(); i += every )
    s2 << s[ i ];
   return s2.str(); 
}


// @param s une chaîne de caractères chiffrée (A-Z).
// @return la fréquence des lettres 'A' à 'Z' (0='A', 25='Z').
std::vector<float> frequencies( const std::string & s ){
    vector<float> result (26,0.0);

    for(char c : s){
        result[(c - 'A')]++;
    }

    for(int i=0; i<26; i++)
        result[i] /= s.length();

    return result;
}

bool isChiffrementMonoAlphabetique(std::string texteChiffre){
    float ic = coincidence(frequencies(texteChiffre));
    float icFrench = coincidence(frequenciesFrench());
    float icRandom = coincidence(frequenciesRandom());

    return abs(ic - icFrench) < abs(ic -icRandom);

}

float coincidence( const std::vector<float> & f ){
    float result = 0.0;
    for(float freq : f)
        result += freq*freq;

    return result;
}

float coincidenceMutuelle( const std::vector<float> & f1,
                           const std::vector<float> & f2,
                           unsigned int shift2 ){
    float result = 0.0;

    for(unsigned int i=0; i<f1.size(); i++){
        result += f1[i] * f2[(i + shift2)%f2.size()];
    }

    return result;
}

ostream& operator<<( ostream &flux, vector<char> const& vec )
{
    for(char c : vec){
        flux << c;
    }
    return flux;
}
