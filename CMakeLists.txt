cmake_minimum_required(VERSION 3.8)
project(info910_tp1_cryptographie)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES
        filter-az.cxx
        freqs.cxx
        freqs.h
        test-freqs.cxx
        utils.cxx
        utils.h
        chiffre-vigenere.cpp check-freqs.cpp detecte-vigenere.cpp)

add_executable(info910_tp1_cryptographie ${SOURCE_FILES})