//
// Created by antoine on 14/09/18.
//

#include "utils.h"
#include "freqs.h"

using namespace std;

int main(int argc, char *argv[]) {
    vector<float> result;
    string text = readInput(cin);
    if(argc == 3)
        text = subtext(text,atoi(argv[1]),atoi(argv[2]));

    result = frequencies(text);

    for(int i=0; i<26; i++)
        cout << (char)('A' + i) << "=" << result[i] << " ";
    cout<<endl;

    cout << "Ic = " << coincidence(result) << " Ifrench=" << coincidence(frequenciesFrench()) << " Irand=" << coincidence(frequenciesRandom())<< endl;

    if(isChiffrementMonoAlphabetique(text))
        cout << "French text or mono-alphabetic ciphered text !" << endl;
    else
        cout << "Likely random text or other cipher !" << endl;
}